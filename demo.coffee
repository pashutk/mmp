fs = require 'fs'
_ = require 'underscore'
Promise = require 'promise'
url = require 'url'
toughCookie = require 'tough-cookie'
request = require 'request'
FileCookieStore = require 'tough-cookie-filestore'
# j = request.jar new FileCookieStore 'cookie.json'
# request = request.defaults
	# jar: j
jquery      = fs.readFileSync './node_modules/jquery/dist/jquery.js'
jsdom       = require 'jsdom'
nicejson2csv = require 'nice-json2csv'
json2xls = require 'json2xls'
json2csv = require 'json2csv'
S = require 'string'
AgentHTTP = require 'socks5-http-client/lib/Agent'
AgentHTTPS = require 'socks5-https-client/lib/Agent'
configFile = fs.readFileSync './config.json'
methods = require './methods'
config = JSON.parse configFile
loginSuccesful = no 
superstop = no
express     = require 'express'
app = express()
http = require 'http'
server = http.Server app
bodyParser  = require 'body-parser'
socketio = require 'socket.io'
io = socketio server
socket = {}

app.set 'port', (process.env.PORT || 5000)
app.set 'view engine', 'jade'
app.set 'title', 'le parser de mymail'
app.use bodyParser.urlencoded
  extended: false
app.use bodyParser.json()
app.use express.static 'public'
app.use json2xls.middleware

app.get '/', (req, res) ->
  res.render 'demo'

app.get '/view', (req, res) ->
	data = fs.readFileSync 'public/demo.json', {encoding: 'utf8'} 
	res.render 'table', {data: data}

app.get '/download', (req, res) ->
	xls = JSON.parse fs.readFileSync 'public/demo.json', {encoding: 'utf8'} 
	res.xls "demo.xls", xls

dev = yes
cld = (message) ->
	if dev then socket.emit 'console', message
	
io.on 'connection', (socket) ->
	socket = socket
	cld = (message) ->
		if dev then socket.emit 'console', message
	console.log 'connection is estabiled'
	socket.on 'disconnect', ->
		console.log 'connection is closed'
	socket.on 'initData', (msg) ->
		superstop = no
		grab msg, cld
	socket.on 'stop', ->
		superstop = yes

grab = (initData, cld) ->
	superstop = no
	sex = initData.sex
	age = initData.age
	fs.writeFileSync 'public/demo.json','', {encoding: 'utf8'}
	cld 'start getting demo data...'
	
	getPackOfUsers = (offset, sex, age) ->
		promise = new Promise (resolve, reject) ->
			request 
				jar: false
				url: 'http://my.mail.ru/cgi-bin/my/ajax?ajax_call=1&func_name=search.get&arg_agerange='+age+'&arg_sex='+sex+'&arg_offset='+offset+'&arg_limit=50&_=1429021288543'
				, (err, res, body) ->
					if superstop then return
					if err 
						reject err
					else
						users = []
						for user in JSON.parse(body)[2].users
							obj = {}
							obj.id = user.ID
							obj.url = 'http://my.mail.ru'+user.Dir
							obj.lastName = user.LastName
							obj.firstName = user.FirstName
							obj.birthday = null
							obj.sex = user.Sex 
							obj.age = user.Age
							obj.country = user.CountryName
							obj.city = user.CityName
							obj.relations = null
							obj.privateInfo = null
							obj.about = null
							obj.education = null
							obj.places = null
							obj.army = null
							obj.email = user.Email
							
							users.push obj
						cld '...'
						resolve users 
		return promise 

	reqs = []
	for i in [0..9]
		offset = i*10
		do (offset, sex, age) ->
			if superstop then return
			reqs.push getPackOfUsers(offset, sex, age)
			offset = null
			sex = null
			age = null
	Promise.all reqs
		.then (res) ->
			if superstop then return
			users = _.flatten res
			fs.writeFileSync 'public/demo.json', JSON.stringify(users), {encoding: 'utf8'}
			cld 'first step done'

			authFormData = 
				page: 'http://my.mail.ru'
				FailPage: 'http://my.mail.ru/cgi-bin/login?fail=1'
				Login: 'romochka.shef@mail.ru'
				Domain: 'mail.ru'
				Password: 'puditipuditi'

			action = 'https://auth.mail.ru/cgi-bin/auth'

			request.post 
				url: action
				# agentClass: AgentHTTPS,
				# agentOptions: 
				# 	socksHost: '92.127.242.185', 
				# 	socksPort: 1095
				# proxy: 'http://212.192.64.125:3128'
				formData: authFormData
				, (err, res, body) ->
					if superstop then return
					if err 
						console.log err
						cld err
					else
						counter = users.length

						getUserInfo = (_user) ->

							promise = new Promise (resolve, reject) ->
								request 
									# jar: true
									# agentClass: AgentHTTP,
									# agentOptions: 
									# 	socksHost: '92.127.242.185', 
									# 	socksPort: 1095
									# proxy: 'http://212.192.64.125:3128'
									url: _user.url+'info'
									# url: 'http://my.mail.ru/mail/pashok.portveshok/info'
									, (err, res, body) ->
										if superstop then return
										if err then console.log err
										if err then reject err
										if !err and res.statusCode == 200
											jsdom.env 
												html: body
												src: [jquery]
												done: (err, window) -> 
													if superstop then return
													if err then console.log 'jsdom err ', err
													if err then reject err
													$ = window.$   

													cld 'parse user '+_user.id+' | left '+counter+' users'

													switch $('.b-common-friends_header:contains("Доступ ограничен")').length
														when 0
															isPrivate = no
														when 1
															isPrivate = yes
														
													if isPrivate
														_user.privateInfo = true
													else 
														_user.privateInfo = false
														userInfo = {}
														parseInfo = ->
															if superstop then return
															$('#centerColumn h1').each (i,e) ->
																header = $(e).text().trim()
																text = ''
																contin = yes
																if $(e).next().length is 1 
																	nextEl = $(e).next() 
																	loopgo = yes
																else
																	if not $(e).parent().hasClass('body-center')
																		nextEl = $(e).parent().next()
																		loopgo = yes
																	else 
																		loopgo = no
																if loopgo
																	loop
																		if (nextEl.length is 0) || (nextEl.find('h1').length is 1) || (nextEl[0].tagName is 'H1')
																			break
																		else
																			text += nextEl.text().trim()
																			text += '\n'
																			nextEl = nextEl.next()
																userInfo[header] = text
														parseInfo()



														elBday = $('.cc_bio dd:contains("День рождения:")').eq(0)
														elBdayText = elBday.next('dt').text().trim()
														if elBday.length is 1
															_user.birthday = methods.parseDate(elBdayText.slice(0, elBdayText.indexOf(',')))

														rels = $('.cc_bio dd:contains("Отношения:")').eq(0)
														relsText = rels.next('dt').text().trim()
														if rels.length is 1
															_user.relations = relsText

														if userInfo['Местоположения пользователя']? then _user.places = S(userInfo['Местоположения пользователя']).collapseWhitespace().s else _user.places = null
														if userInfo['Образование']? then _user.education = S(userInfo['Образование']).collapseWhitespace().s else _user.education = null
														# if userInfo['Карьера']? then users[userindex].about = userInfo['Карьера']
														if userInfo['Армия']? then _user.army = S(userInfo['Армия']).collapseWhitespace().s else _user.army = null
														if userInfo['Информация о пользователе']? then _user.about = S(userInfo['Информация о пользователе']).collapseWhitespace().s else _user.about = null

														_user.done = true


													counter--
													if counter is 50
														console.log 50
													resolve _user
													window = null
													_user = null
										err = null
										res = null
										body = null

													# fs.writeFileSync 'public/demo.json', JSON.stringify(users), {encoding: 'utf8'}
							return promise 

						_users = []
						for __user in users
							temp = do (__user) ->
								if superstop then return
								_users.push getUserInfo(__user)
								__user = null
							temp = null
						Promise.all _users
							.then (res) ->
								cld 'READY'
								fs.writeFileSync 'public/demo.json', JSON.stringify(res), {encoding: 'utf8'}
								global.gc()



process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

server.listen app.get('port'), ->
  host = server.address().address
  port = server.address().port
  console.log 'Example app listening at http://%s:%s', host, port

