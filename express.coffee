fs = require 'fs'
clco = ->
	fs.writeFileSync 'public/cookie.json', JSON.stringify({}), {encoding: 'utf8'}
	fs.writeFileSync 'cookie.json', JSON.stringify({}), {encoding: 'utf8'}
express     = require 'express'
app = express()
http = require 'http'
server = http.Server app
bodyParser  = require 'body-parser'
socketio = require 'socket.io'
io = socketio server
socket = {}

app.set 'port', (process.env.PORT || 5000)
app.set 'view engine', 'jade'
app.set 'title', 'le parser de mymail'
app.use bodyParser.urlencoded
  extended: false
app.use bodyParser.json()
app.use express.static 'public'


app.get '/', (req, res) ->
  res.render 'index'

app.post '/get', (req, res) ->
	console.log req.body.users
	res.send({msg: "server get your msg and send them to you", data: req.body.users})

dev = yes
cld = (message) ->
	if dev then socket.emit 'console', message
	

_ = require 'underscore'
Promise = require 'promise'
url = require 'url'
toughCookie = require 'tough-cookie'
request = require 'request'
FileCookieStore = require 'tough-cookie-filestore'
j = request.jar new FileCookieStore 'cookie.json'
request = request.defaults
	jar: j
jquery      = fs.readFileSync './node_modules/jquery/dist/jquery.js'
jsdom       = require 'jsdom'
nicejson2csv = require 'nice-json2csv'
app.use nicejson2csv.expressDecorator
json2xls = require 'json2xls'
json2csv = require 'json2csv'
S = require 'string'
AgentHTTP = require 'socks5-http-client/lib/Agent'
AgentHTTPS = require 'socks5-https-client/lib/Agent'
configFile = fs.readFileSync './config.json'
methods = require './methods'
config = JSON.parse configFile
loginSuccesful = no 
superstop = no


app.get '/getCsv', (req, res) ->
	csv = JSON.parse fs.readFileSync 'public/res.json', {encoding: 'utf8'} 
	res.csv csv, "res.csv"

io.on 'connection', (socket) ->
	socket = socket
	cld = (message) ->
		if dev then socket.emit 'console', message
	console.log 'connection is estabiled'
	socket.on 'disconnect', ->
		console.log 'connection is closed'
	socket.on 'initData', (msg) ->
		superstop = no
		_proxies = msg.proxies
		if msg.proxies
			application config, cld, methods.parseUsersList(msg.users), methods.parseProxiesList(msg.proxies)
		else 
			cld 'fetching proxies...'
			fs.readFile 'public/exampleproxies.txt', {encoding: 'utf8'}, (err,body) ->
				cld body
				if err then cld err
				if body.length is 0 
					cld '0 PROXIES NO PROXIES GETTED TRY TO SET PROXIES MANUALLY'
					return
				else
					cld 'grabed '+methods.parseProxiesList(body).length+' proxies' 
					application config, cld, methods.parseUsersList(msg.users), methods.parseProxiesList(body)	
	socket.on 'getInit', ->
		getInitData cld
	socket.on 'reset', ->
		emptyArr = []
		fs.writeFileSync 'public/res.json', JSON.stringify(emptyArr), {encoding: 'utf8'}
		fs.writeFileSync 'public/res.csv', JSON.stringify(emptyArr), {encoding: 'utf8'}
		fs.writeFileSync 'public/toParse.json', JSON.stringify(emptyArr), {encoding: 'utf8'}
		fs.writeFileSync 'public/cookie.json', JSON.stringify(emptyArr), {encoding: 'utf8'}
		cld 'RESET COMPLETE'
	socket.on 'stop', ->
		superstop = yes




# request.get config.paths.proxiesRemote, (err,resp,body) ->
# 	if err
# 		dptcher.proxies = methods.parseProxiesList fs.readFileSync config.paths.proxies, {encoding: 'utf8'}
# 	else
# 		dptcher.proxies = methods.parseProxiesList body
# 	app config
dptcher = {}

application = (config, cld, users, proxies) ->
	cld 'App is running'
	dptcher.toParse = JSON.parse fs.readFileSync 'public/toParse.json', {encoding: 'utf8'}
	dptcher.newToParse = []
	dptcher.logins = users
	dptcher.proxies = proxies
	emptyArr = []
	fs.writeFileSync 'public/res.json', JSON.stringify(emptyArr), {encoding: 'utf8'}
	fs.writeFileSync 'public/res.csv', JSON.stringify(emptyArr), {encoding: 'utf8'}
	do startParsing 


startParsing = ->
	do clco
	if not dptcher.toParse.length
		cld 'FIRST GET INIT DATA'
		return
	if superstop then return
	cld 'Start parsing'
	randomProxy = methods.getRandomProxy dptcher.proxies
	randomUser = methods.getRandomUser dptcher.logins

	dptcher.toParse = _.first dptcher.toParse, 150

	login randomProxy, randomUser, (loginedOpts) ->
		if superstop then return
		cld 'Logined as user '+loginedOpts.user.login
		temp = ->
			if superstop then return

			currentUser = dptcher.toParse[0]
			cld 'Left '+dptcher.toParse.length+' users'

			do (currentUser, loginedOpts) ->
				if superstop then return

				getUserFriends currentUser, loginedOpts, (friends) ->
					if superstop then return
					
					dptcher.newToParse.push friends
					dptcher.newToParse = _.flatten dptcher.newToParse
					dptcher.newToParse = _.uniq dptcher.newToParse
					fs.writeFileSync 'public/newToParse.json', JSON.stringify(dptcher.newToParse), {encoding: 'utf8'}

					_userInfo = getUserInfo currentUser, loginedOpts
					_userInfo.then (user) ->
						if superstop then return
						cld 'userinfo getted'
						_users = JSON.parse fs.readFileSync 'public/res.json', {encoding: 'utf8'}
						_users.push user
						fs.writeFileSync 'public/res.json', JSON.stringify(_users), {encoding: 'utf8'}
						dptcher.toParse.shift()
						cld 'userinfo writted to file'
						temp()
					, ->
						setTimeout ->
							if superstop then return
							startParsing()
						, 3000

		temp()
			



login = (proxy, user, cb) ->
	if superstop then return
	cld 'Trying to login as user '+user.login
	cld 'Used proxy is '+proxy.host+':'+proxy.port
	
	authFormData = 
		page: config.authData.authPage
		FailPage: config.authData.failPage
		Login: user.login
		Domain: config.authData.authDomain
		Password: user.password


	request
		method: 'POST'
		url: url.parse config.authData.postActionUrl
		# timeout: config.timings.loginTimeout
		# agentClass: AgentHTTPS
		# agentOptions: 
		# 	socksHost: proxy.host 
		# 	socksPort: proxy.port
		formData: authFormData
		, (err, res, body) ->
			if superstop then return
			fs.writeFileSync 'post.err.json', JSON.stringify(err), {encoding: 'utf8'}
			fs.writeFileSync 'post.res.json', JSON.stringify(res), {encoding: 'utf8'}
			fs.writeFileSync 'post.body.html', body, {encoding: 'utf8'}
			if err 
				cld err
				cld 'Login POST request error'
				setTimeout ->
					do startParsing
				, config.timings.reloginTimeout
			else 
				loginSuccesful = yes
				loginedOpts =
					response: res
					body: body
					proxy: proxy
					user: user
				cb loginedOpts

getUserFriends = (userUrl, loginedOpts, cb) ->
	if superstop then return
	_url = url.resolve userUrl, 'friends'
	cld 'frinds req '+_url
	request
		method: 'GET'
		# agentClass: AgentHTTP,
		# agentOptions: 
		# 	socksHost: loginedOpts.proxy.host
		# 	socksPort: loginedOpts.proxy.port
		url: _url
		, (err, res, body) ->
			if superstop then return
			if err 
				cld err
				cld 'GettingFriends request error'
				setTimeout ->
					do startParsing
				, config.timings.reloginTimeout
			else 
				cld 'getting '+_url+' user friends'
				jsdom.env 
					html: body
					src: [jquery]
					done: (err, window) -> 
						if superstop then return
						if err 
							cld 'jsdom error'
							cld err 
							getUserFriends userUrl, loginedOpts, cb
						else 
							$ = window.$
							if $('#login_page').length
								cld 'fuck it dsnt wrk'
								setTimeout ->
									getUserFriends userUrl, loginedOpts, cb
								, config.timings.reloginTimeout
							else
								cld 'jsdom parse successfully'
								friends = []
								$('.b-catalog__friends-item')
									.each (i,e) ->
										friends.push url.resolve 'http://my.mail.ru/friends', $(e).find('.b-catalog__friends-item-left a').attr('href')
								cld 'user has '+friends.length+' friends'
								cb friends

getUserInfo = (userLink, loginedOpts) ->
	if superstop then return
	cld 'Start getting user info'
	promise = new Promise (resolve, reject) ->
		if superstop then return
		cld 'Making req to '+ url.resolve userLink, 'info'
		request 
			method: 'GET'
			# agentClass: AgentHTTP,
			# agentOptions: 
			# 	socksHost: loginedOpts.proxy.host 
			# 	socksPort: loginedOpts.proxy.port
			url: url.resolve userLink, 'info'
			timeout: 5000
			, (err, res, body) ->
				if superstop then return
				if err
					cld 'Cannot load user info page'
					cld err 
					reject err
				if !err and res.statusCode == 200
					cld userLink
					jsdom.env 
						html: body
						src: [jquery]
						done: (err, window) -> 
							if superstop then return
							if err 
								cld 'jsdom err ', err
								reject err
							$ = window.$ 
							user = {}  

							switch $('.b-common-friends_header:contains("Доступ ограничен")').length
								when 0
									isPrivate = no
								when 1
									isPrivate = yes

							info = JSON.parse $('script[data-mru-fragment="models/user/journal"]')[0].text
							user.firstName = info.name.slice(0,info.name.indexOf(' ')).trim()
							user.secondName = info.name.slice(info.name.indexOf(' '), info.name.length).trim()
							user.email = info.email
							user.id = info.id
							user.url = url.resolve 'http://my.mail.ru/', info.dir
							user.isVip = info.isVip
								
							if isPrivate
								user.privateInfo = true
							else 
								user.privateInfo = false
								userInfo = {}
								parseInfo = ->
									$('#centerColumn h1').each (i,e) ->
										header = $(e).text().trim()
										text = ''
										contin = yes
										if $(e).next().length is 1 
											nextEl = $(e).next() 
											loopgo = yes
										else
											if not $(e).parent().hasClass('body-center')
												nextEl = $(e).parent().next()
												loopgo = yes
											else 
												loopgo = no
										if loopgo
											loop
												# console.log nextEl.length
												# console.log '==============='
												if (nextEl.length is 0) || (nextEl.find('h1').length is 1) || (nextEl[0].tagName is 'H1')
													return
												else
													text += nextEl.text().trim()
													text += '\n'
													nextEl = nextEl.next()
										userInfo[header] = text
								parseInfo()




								from = $('.cc_bio dd:contains("Откуда:")').eq(0)
								fromText = from.next('dt').text().trim()
								if from.length is 1
									user.from = fromText

								elBday = $('.cc_bio dd:contains("День рождения:")').eq(0)
								elBdayText = elBday.next('dt').text().trim()
								if elBday.length is 1
									user.birthday = elBdayText.slice 0, elBdayText.indexOf ','
									user.birthday = methods.parseDate user.birthday

								rels = $('.cc_bio dd:contains("Отношения:")').eq(0)
								relsText = rels.next('dt').text().trim()
								if rels.length is 1
									user.relations = relsText

								if userInfo['Местоположения пользователя']? then user.places = S(userInfo['Местоположения пользователя']).collapseWhitespace().s else user.places = null
								if userInfo['Образование']? then user.education = S(userInfo['Образование']).collapseWhitespace().s else user.education = null
								# if userInfo['Карьера']? then users[userindex].about = userInfo['Карьера']
								if userInfo['Армия']? then user.army = S(userInfo['Армия']).collapseWhitespace().s else user.army = null
								if userInfo['Информация о пользователе']? then user.about = S(userInfo['Информация о пользователе']).collapseWhitespace().s else user.about = null

								user.done = true



							resolve user

	return promise

getInitData = (cld) ->
	fs.writeFileSync 'public/toParse.json','', {encoding: 'utf8'}
	cld 'start getting init data...'
	
	getPackOfUsers = (offset) ->
		promise = new Promise (resolve, reject) ->
			request 
				url: 'http://my.mail.ru/cgi-bin/my/ajax?ajax_call=1&func_name=search.get&arg_sex=2&arg_offset='+offset+'&arg_limit=50&_=1429021288543'
				, (err, res, body) ->
					if err 
						reject err
					else
						users = []
						for i in JSON.parse(body)[2].users
							users.push url.resolve 'http://my.mail.ru/', i.Dir 
						resolve users 
		return promise 

	reqs = []
	for i in [0..99]
		offset = i*10
		reqs.push getPackOfUsers offset
	Promise.all reqs
		.then (res) ->
			urls = _.flatten res
			fs.writeFileSync 'public/toParse.json', JSON.stringify(urls), {encoding: 'utf8'}
			cld 'init data recieve successful'
			cld 'list of users intended for parsing is available here: /toParse.json'

		
			



process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

server.listen app.get('port'), ->
  host = server.address().address
  port = server.address().port
  console.log 'Example app listening at http://%s:%s', host, port
