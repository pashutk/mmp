console.log('script.js is loaded');
var socket = io();
$('#start').click(function(){
	var obj = {users: document.getElementById('users').value, proxies: document.getElementById('proxies').value};
	console.log(obj);
	socket.emit('initData', obj)
});
$('#init').click(function(){
	socket.emit('getInit', 'go');
});
$('#reset').click(function(){
	socket.emit('reset', 'go');
});
$('#stop').click(function(){
	socket.emit('stop', 'go');
});
var out = $('#output')
out.change(function() {
	out.scrollTop(out[0].scrollHeight);
});
socket.on('console', function(msg){
	console.log(msg);
	out.val(out.val()+msg+'\n');
	if (out.scrollTop()>out.scrollTop(out[0].scrollHeight)-100){
		out.scrollTop(out[0].scrollHeight);
	}
});
