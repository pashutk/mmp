console.log('script.js is loaded');
var socket = io();
$('#start').click(function(){
	var obj = {
		sex: document.getElementById('sex').value,
		age: document.getElementById('age').value
	};
	console.log(obj);
	socket.emit('initData', obj)
});
var out = $('#output')
out.change(function() {
	out.scrollTop(out[0].scrollHeight);
});
socket.on('console', function(msg){
	console.log(msg);
	out.val(out.val()+msg+'\n');
	if (out.scrollTop()>out.scrollTop(out[0].scrollHeight)-100){
		out.scrollTop(out[0].scrollHeight);
	}
});
