XRegExp     = require 'xregexp'
  .XRegExp
moment = require 'moment'
moment.locale 'ru-months', 
	months: 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря'.split ','



getRandomElement = (arr) ->
	return arr[Math.floor(Math.random()*arr.length)]

module.exports = 

	parseDate: (string) ->
		return moment string, 'DD MMMM YYYY', 'ru-months'
			.format 'YYYY-MM-DD'

	
	parseProxiesList: (txt) ->
		list = []
		reg = /(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\:(\d*)/
		XRegExp.forEach txt, reg, (match, i) ->
			proxy = 
				host: match[1]
				port: match[2]
			list.push proxy

		return list

	parseUsersList: (txt) ->
		list = []
		reg = /(\S+):(\S+)\s*/
		XRegExp.forEach txt, reg, (match, i) ->
			user = 
				login: match[1]
				password: match[2]
			list.push user

		return list

	getRandomProxy: (list) ->
		return getRandomElement list

	getRandomUser: (list) ->
		return getRandomElement list
		